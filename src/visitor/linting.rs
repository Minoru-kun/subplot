use crate::panhelper;
use crate::SubplotError;

use pandoc_ast::{Block, MutVisitor};

#[derive(Default)]
pub struct LintingVisitor {
    pub issues: Vec<SubplotError>,
}

impl MutVisitor for LintingVisitor {
    fn visit_vec_block(&mut self, vec_block: &mut Vec<Block>) {
        for block in vec_block {
            match block {
                Block::CodeBlock(attr, _) => {
                    if panhelper::is_class(attr, "file") {
                        let newlines: Vec<_> =
                            panhelper::find_attr_kv(&attr, "add-newline").collect();
                        match newlines.len() {
                            0 => {}
                            1 => match newlines[0].to_ascii_lowercase().as_ref() {
                                "auto" | "yes" | "no" => {}
                                _ => self.issues.push(SubplotError::UnrecognisedAddNewline(
                                    panhelper::get_filename(&attr),
                                    newlines[0].to_owned(),
                                )),
                            },
                            _ => self.issues.push(SubplotError::RepeatedAddNewlineAttribute(
                                panhelper::get_filename(&attr),
                            )),
                        }
                    }
                }
                _ => {
                    self.visit_block(block);
                }
            }
        }
    }
}
