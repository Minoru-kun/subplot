use subplotlib::prelude::*;

// --------------------------------

lazy_static! {
    static ref SUBPLOT_EMBEDDED_FILES: Vec<SubplotDataFile> = vec![SubplotDataFile::new(
        "ZXJyLnNo",
        "IyEvYmluL3NoCmVjaG8gIiRAIiAxPiYyCg=="
    ),];
}

// ---------------------------------

// Successful execution
#[test]
fn successful_execution() {
    let mut scenario = Scenario::new(&base64_decode("U3VjY2Vzc2Z1bCBleGVjdXRpb24="));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/true"
            &base64_decode("L2Jpbi90cnVl"),
        )
        .args(
            // ""
            &base64_decode(""),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is::Builder::default()
        .exit(0)
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is_zero::Builder::default().build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Successful execution in a sub-directory
#[test]
fn successful_execution_in_a_sub_directory() {
    let mut scenario = Scenario::new(&base64_decode(
        "U3VjY2Vzc2Z1bCBleGVjdXRpb24gaW4gYSBzdWItZGlyZWN0b3J5",
    ));

    let step = subplotlib::steplibrary::files::make_directory::Builder::default()
        .path(
            // "xyzzy"
            &base64_decode("eHl6enk="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run_in::Builder::default()
        .dirname(
            // "xyzzy"
            &base64_decode("eHl6enk="),
        )
        .argv0(
            // "/bin/pwd"
            &base64_decode("L2Jpbi9wd2Q="),
        )
        .args(
            // ""
            &base64_decode(""),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is::Builder::default()
        .exit(0)
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is_zero::Builder::default().build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_contains::Builder::default()
        .text(
            // "/xyzzy"
            &base64_decode("L3h5enp5"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Failed execution
#[test]
fn failed_execution() {
    let mut scenario = Scenario::new(&base64_decode("RmFpbGVkIGV4ZWN1dGlvbg=="));

    let step = subplotlib::steplibrary::runcmd::try_to_run::Builder::default()
        .argv0(
            // "/bin/false"
            &base64_decode("L2Jpbi9mYWxzZQ=="),
        )
        .args(
            // ""
            &base64_decode(""),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is_not::Builder::default()
        .exit(0)
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is_nonzero::Builder::default().build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Failed execution in a sub-directory
#[test]
fn failed_execution_in_a_sub_directory() {
    let mut scenario = Scenario::new(&base64_decode(
        "RmFpbGVkIGV4ZWN1dGlvbiBpbiBhIHN1Yi1kaXJlY3Rvcnk=",
    ));

    let step = subplotlib::steplibrary::files::make_directory::Builder::default()
        .path(
            // "xyzzy"
            &base64_decode("eHl6enk="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::try_to_run_in::Builder::default()
        .dirname(
            // "xyzzy"
            &base64_decode("eHl6enk="),
        )
        .argv0(
            // "/bin/false"
            &base64_decode("L2Jpbi9mYWxzZQ=="),
        )
        .args(
            // ""
            &base64_decode(""),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is_not::Builder::default()
        .exit(0)
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is_nonzero::Builder::default().build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stdout is exactly as wanted
#[test]
fn check_stdout_is_exactly_as_wanted() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3Rkb3V0IGlzIGV4YWN0bHkgYXMgd2FudGVk",
    ));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/echo"
            &base64_decode("L2Jpbi9lY2hv"),
        )
        .args(
            // " hello, world"
            &base64_decode("IGhlbGxvLCB3b3JsZA=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_is::Builder::default()
        .text(
            // "hello, world\n"
            &base64_decode("aGVsbG8sIHdvcmxkXG4="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stderr is exactly as wanted
#[test]
fn check_stderr_is_exactly_as_wanted() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3RkZXJyIGlzIGV4YWN0bHkgYXMgd2FudGVk",
    ));

    let step = subplotlib::steplibrary::runcmd::helper_script::Builder::default()
        .script({
            use std::path::PathBuf;
            // err.sh
            let target_name: PathBuf = base64_decode("ZXJyLnNo").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "sh"
            &base64_decode("c2g="),
        )
        .args(
            // " err.sh hello, world"
            &base64_decode("IGVyci5zaCBoZWxsbywgd29ybGQ="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stderr_is::Builder::default()
        .text(
            // "hello, world\n"
            &base64_decode("aGVsbG8sIHdvcmxkXG4="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stdout using sub-string search
#[test]
fn check_stdout_using_sub_string_search() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3Rkb3V0IHVzaW5nIHN1Yi1zdHJpbmcgc2VhcmNo",
    ));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/echo"
            &base64_decode("L2Jpbi9lY2hv"),
        )
        .args(
            // " hello, world"
            &base64_decode("IGhlbGxvLCB3b3JsZA=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_contains::Builder::default()
        .text(
            // "world\n"
            &base64_decode("d29ybGRcbg=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::exit_code_is::Builder::default()
        .exit(0)
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stderr using sub-string search
#[test]
fn check_stderr_using_sub_string_search() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3RkZXJyIHVzaW5nIHN1Yi1zdHJpbmcgc2VhcmNo",
    ));

    let step = subplotlib::steplibrary::runcmd::helper_script::Builder::default()
        .script({
            use std::path::PathBuf;
            // err.sh
            let target_name: PathBuf = base64_decode("ZXJyLnNo").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "sh"
            &base64_decode("c2g="),
        )
        .args(
            // " err.sh hello, world"
            &base64_decode("IGVyci5zaCBoZWxsbywgd29ybGQ="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stderr_contains::Builder::default()
        .text(
            // "world\n"
            &base64_decode("d29ybGRcbg=="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stdout using regular expressions
#[test]
fn check_stdout_using_regular_expressions() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3Rkb3V0IHVzaW5nIHJlZ3VsYXIgZXhwcmVzc2lvbnM=",
    ));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/echo"
            &base64_decode("L2Jpbi9lY2hv"),
        )
        .args(
            // " hello, world"
            &base64_decode("IGhlbGxvLCB3b3JsZA=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_matches_regex::Builder::default()
        .regex(
            // "world$"
            &base64_decode("d29ybGQk"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stderr using regular expressions
#[test]
fn check_stderr_using_regular_expressions() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3RkZXJyIHVzaW5nIHJlZ3VsYXIgZXhwcmVzc2lvbnM=",
    ));

    let step = subplotlib::steplibrary::runcmd::helper_script::Builder::default()
        .script({
            use std::path::PathBuf;
            // err.sh
            let target_name: PathBuf = base64_decode("ZXJyLnNo").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "sh"
            &base64_decode("c2g="),
        )
        .args(
            // " err.sh hello, world"
            &base64_decode("IGVyci5zaCBoZWxsbywgd29ybGQ="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stderr_matches_regex::Builder::default()
        .regex(
            // "world$"
            &base64_decode("d29ybGQk"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stdout is not exactly something
#[test]
fn check_stdout_is_not_exactly_something() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3Rkb3V0IGlzIG5vdCBleGFjdGx5IHNvbWV0aGluZw==",
    ));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/echo"
            &base64_decode("L2Jpbi9lY2hv"),
        )
        .args(
            // " hi"
            &base64_decode("IGhp"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_isnt::Builder::default()
        .text(
            // "hello, world\n"
            &base64_decode("aGVsbG8sIHdvcmxkXG4="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stderr is not exactly something
#[test]
fn check_stderr_is_not_exactly_something() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3RkZXJyIGlzIG5vdCBleGFjdGx5IHNvbWV0aGluZw==",
    ));

    let step = subplotlib::steplibrary::runcmd::helper_script::Builder::default()
        .script({
            use std::path::PathBuf;
            // err.sh
            let target_name: PathBuf = base64_decode("ZXJyLnNo").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "sh"
            &base64_decode("c2g="),
        )
        .args(
            // " err.sh hi"
            &base64_decode("IGVyci5zaCBoaQ=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stderr_isnt::Builder::default()
        .text(
            // "hello, world\n"
            &base64_decode("aGVsbG8sIHdvcmxkXG4="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stdout doesn’t contain sub-string
#[test]
fn check_stdout_doesn_t_contain_sub_string() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3Rkb3V0IGRvZXNu4oCZdCBjb250YWluIHN1Yi1zdHJpbmc=",
    ));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/echo"
            &base64_decode("L2Jpbi9lY2hv"),
        )
        .args(
            // " hi"
            &base64_decode("IGhp"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_doesnt_contain::Builder::default()
        .text(
            // "world"
            &base64_decode("d29ybGQ="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stderr doesn’t contain sub-string
#[test]
fn check_stderr_doesn_t_contain_sub_string() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3RkZXJyIGRvZXNu4oCZdCBjb250YWluIHN1Yi1zdHJpbmc=",
    ));

    let step = subplotlib::steplibrary::runcmd::helper_script::Builder::default()
        .script({
            use std::path::PathBuf;
            // err.sh
            let target_name: PathBuf = base64_decode("ZXJyLnNo").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "sh"
            &base64_decode("c2g="),
        )
        .args(
            // " err.sh hi"
            &base64_decode("IGVyci5zaCBoaQ=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stderr_doesnt_contain::Builder::default()
        .text(
            // "world"
            &base64_decode("d29ybGQ="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stdout doesn’t match regular expression
#[test]
fn check_stdout_doesn_t_match_regular_expression() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3Rkb3V0IGRvZXNu4oCZdCBtYXRjaCByZWd1bGFyIGV4cHJlc3Npb24=",
    ));

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "/bin/echo"
            &base64_decode("L2Jpbi9lY2hv"),
        )
        .args(
            // " hi"
            &base64_decode("IGhp"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stdout_doesnt_match_regex::Builder::default()
        .regex(
            // "world$"
            &base64_decode("d29ybGQk"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Check stderr doesn’t match regular expressions
#[test]
fn check_stderr_doesn_t_match_regular_expressions() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q2hlY2sgc3RkZXJyIGRvZXNu4oCZdCBtYXRjaCByZWd1bGFyIGV4cHJlc3Npb25z",
    ));

    let step = subplotlib::steplibrary::runcmd::helper_script::Builder::default()
        .script({
            use std::path::PathBuf;
            // err.sh
            let target_name: PathBuf = base64_decode("ZXJyLnNo").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::run::Builder::default()
        .argv0(
            // "sh"
            &base64_decode("c2g="),
        )
        .args(
            // " err.sh hi"
            &base64_decode("IGVyci5zaCBoaQ=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::runcmd::stderr_doesnt_match_regex::Builder::default()
        .regex(
            // "world$"
            &base64_decode("d29ybGQk"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}
