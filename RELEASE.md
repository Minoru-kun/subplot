---
title: Release process for Subplot
...

The subplot project consists of serveral crates, any step which refers to the
`Cargo.toml` or `Cargo.lock` files should be repeated for all crates. Currently
the set of crates to consider for release is:

- `subplotlib-derive`
- `subplotlib`
- `subplot`

When determining if a new version of a crate is required, use `git log` with
appropriate filters.

Before any release of Subplot, it is appropriate to ensure that versions of
dependencies have been updated where needed. You may want to do this on a
different branch, or as part of the release branch.

Follow these steps to make a release of Subplot

1. Create a branch on which to do the release (e.g. call it `release`)
2. Ensure dependencies are up-to-date, for example, `cargo update` and its
   associated tool `cargo outdated` will be of help.
3. Update `NEWS.md` as appropriate
4. Update the version numbers everywhere they need to be updated. Use
   [semantic versioning](https://semver.org/) appropriately. You may use
   the [bumper](https://bumper.liw.fi/) tool to do this.
   - `NEWS.md`
   - `Cargo.toml`
   - `debian/changelog`
5. Ensure everything is appropriately committed with good messages.
6. Run `cargo publish --dry-run` for each crate as needed, to ensure that
   we will be okay to publish the crates later.
7. Push the branch to Gitlab and create the merge request.
8. Have someone review, approve (assuming green CI), and merge the release
   branch.
9. Create a signed, annotated, Git tag `vX.Y.Z` for version `X.Y.Z` of Subplot.
10. Push the tag to Gitlab and to `git.liw.fi`
11. Publish any changed crates to `crates.io` with `cargo publish` in the
    relevant directories.
12. Announce our jubilation to the world.
