---
title: Release notes for Subplot
...

This file summarises the changes between released versions of Subplot and its
associated libraries.

# Version 0.2.1, released 2021-07-11

This is the first release meant for use by people other than Subplot's
own developers.

* The locations of the `dot` and `plantuml` programs and for the Java
  byte code interpreter can now be configured when Subplot is invoked,
  which is useful when they're not installed in the locations where
  Debian puts them. Those programs are used by Subplot to render
  diagrams.

* The `./check` script now outputs the last one hundred lines or so of
  the log file produced by the generated test program, if that program
  fails. This makes it easier to debug failures under CI.

* Additionally, there have been some minor tweaks only visible to
  those developing Subplot itself.

# Version 0.2.0, released 2021-06-12

This is the first public release of Subplot. No APIs or other surfaces are to be
considered stable at this time. While the `subplotlib` and `subplotlib-derive`
crates have been published, they do not form part of this release.

The only _template_ which is considered in any sense "supported" in this release
is the `python` template.
